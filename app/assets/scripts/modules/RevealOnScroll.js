import $ from 'jquery';
import waypoints from "../../../../node_modules/waypoints/lib/noframework.waypoints";
class RevealOnScroll{
    constructor(els,offsetPercentage = "60%"){
        this.itemsToReveal = els;
        this.offsetPercentage = offsetPercentage;
        this.hideInitially();
        this.createWaypoints();
    }
    hideInitially(){
        console.log("Called");
        this.itemsToReveal.addClass("reveal-item");
        //this.itemToReveal.classList.add("reveal-item");
    }
    createWaypoints(){
        console.log("Created");
        var that = this;
        this.itemsToReveal.each(function(){//yahya i.e. each function k anadar this points to every element
            var currentElement = this;
            new Waypoint({
                element: currentElement,
                handler: function(){
                    $(currentElement).addClass("reveal-item-is-visible");
                },
                offset: that.offsetPercentage
            });
        })
    }
}
export default RevealOnScroll;